# Enlace Turnero y Pedidos

Servicio de turnos y pedidos con aprobación de pedidos (con marca de agua).  

## Servicio de turnos para entidades
En la pagina principal podes solicitar un turno. El sitio, a medida que se van generando, te va dejando solo los horarios disponibles.
Cuando cambias de dia el sitio de actualiza automaticamente gracias al complemento Laravel LiveWire.

## Aprobacion de certificados o documentos en PDF
El usuario sube un documento en PDF y el administrador puede leer y generar un nuevo archivo con firma o marca de agua aduciendo que ese documento esta aprobado.  
