<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Permiso;
Use App\Http\Controllers\UsuarioController;


class PermisoController extends Controller
{
    public function nuevoPermiso ()
    {
        $usuarioID = auth()->id();
        //$usu = Session::all();
        $usuario = User::find($usuarioID);
        if ($usuario->rol == 'usuario')
        {
            return view ('usuario.permisos.nuevo-permiso', ['usuario'=>$usuario] );
        }
        elseif ($usuario->rol == 'admin')
        {
            //return view ('admin.lista-de-tramites');
            //return redirect ('/adminTramites');
            return redirect()->action([UsuarioController::class, 'adminDatosUsuario']);
        }
        else
        {
            return "No tiene un rol válido";
        }
    }

    public function recibePermiso (Request $request)
    {
        //$usuarioID = auth()->id();
        $permisoNuevo = request()->except('_token');
        $permisoNuevo['created_at'] = date('Y-m-d H:i:s');
        Permiso::insert($permisoNuevo);
        $datos = "Solicitud de Permiso cargado";

        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        
        return view ('usuario.permisos.nuevo-permiso', ['usuario'=>$usuario, 'datos'=>$datos] );
    }

    public function misPermisos ()
    {
        $usuarioID = auth()->id();
        $tramites = Permiso::where('id_usuario', $usuarioID)->get();
        //dd($tramites);
        return view ('usuario.permisos.mis-permisos', ['tramites'=>$tramites]);
    }

    public function modificaTramitePermiso (Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Permiso::find ($idTramite);
        
        return view ('usuario.permisos.modificar-tramite-permiso', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramitePermiso (Request $request)
    {
        $request->validate([    
            'archivo_adjunto'=>'|file|max:5000|mimes:pdf'
        ]);

        $permisoModifica = request()->except('_token');
        //$permisoModifica = request()->all();
        //dd($permisoModifica);
       
        $actualizaPermiso = Permiso::find($request->get('idTramite'));

        if (isset($permisoModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $permisoModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaPermiso->archivo_adjunto =  $permisoModifica['archivo_adjunto'];
        }

        if (isset($permisoModifica['permiso_desde']) && isset($permisoModifica['permiso_hasta'])  )
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['permiso_desde'];
            $actualizaPermiso->permiso_hasta = $permisoModifica['permiso_hasta'];
            $actualizaPermiso->save();
        }
        elseif (!isset($permisoModifica['permiso_desde']) && isset($permisoModifica['permiso_hasta'])  )
        {
            $actualizaPermiso->permiso_hasta = $permisoModifica['permiso_hasta'];
            $actualizaPermiso->save();
        }
        elseif (isset($permisoModifica['permiso_desde']) && !isset($permisoModifica['permiso_hasta'])  )
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['permiso_desde'];
            $actualizaPermiso->save();
        }else
        {
            $actualizaPermiso->save();
        }
        
        

        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');

        $tramite = Permiso::find ($idTramite);
        return view ('usuario.permisos.modificar-tramite-permiso', ['tramite'=>$tramite, 'datos'=>$datos]);
            //return "En proceso...";

        //Cargar el archivo y despues hacer update en la base
    }
}
