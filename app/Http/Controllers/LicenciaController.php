<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Licencia;
Use App\Http\Controllers\UsuarioController;

class LicenciaController extends Controller
{
    public function nuevaLicencia ()
    {

        $usuarioID = auth()->id();
        //$usu = Session::all();
        $usuario = User::find($usuarioID);
        if ($usuario->rol == 'usuario')
        {
            return view ('usuario.licencias.nueva-licencia', ['usuario'=>$usuario] );
        }
        elseif ($usuario->rol == 'admin')
        {
            //return view ('admin.lista-de-tramites');
            //return redirect ('/adminTramites');
            return redirect()->action([UsuarioController::class, 'adminDatosUsuario']);
        }
        else
        {
            return "No tiene un rol válido";
        }
    }

    public function recibeLicencia (Request $request)
    {
        //$usuarioID = auth()->id();
        $licenciaNueva = request()->except('_token');
        $licenciaNueva['created_at'] = date('Y-m-d H:i:s');
        Licencia::insert($licenciaNueva);
        $datos = "Licencia cargada";

        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        
        return view ('usuario.licencias.nueva-licencia', ['usuario'=>$usuario, 'datos'=>$datos] );
    }

    public function misLicencias ()
    {
        $usuarioID = auth()->id();
        $tramites = Licencia::where('id_usuario', $usuarioID)->get();
        //dd($tramites);
        return view ('usuario.licencias.mis-licencias', ['tramites'=>$tramites]);
    }

    public function modificaTramiteLicencia (Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Licencia::find ($idTramite);
        
        return view ('usuario.licencias.modificar-tramite-licencia', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramiteLicencia (Request $request)
    {
        $request->validate([    
            'archivo_adjunto'=>'|file|max:5000|mimes:pdf'
        ]);

        $licenciaModifica = request()->except('_token');
        //$licenciaModifica = request()->all();
        //dd($licenciaModifica);
       
        $actualizaTramite = Licencia::find($request->get('idTramite'));

        if (isset($licenciaModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $licenciaModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaTramite->archivo_adjunto =  $licenciaModifica['archivo_adjunto'];
        }

        if (isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->save();
        }
        elseif (!isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->save();
        }
        elseif (isset($licenciaModifica['licencia_desde']) && !isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->save();
        }else
        {
            $actualizaTramite->save();
        }
        
        

        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');

        $tramite = Licencia::find ($idTramite);
        return view ('usuario.licencias.modificar-tramite-licencia', ['tramite'=>$tramite, 'datos'=>$datos]);
            return "En proceso...";

        //Cargar el archivo y despues hacer update en la base
    }






    public function aprobarLicencia ($id)
    {
        //comando prueba
        //$comando = "./markpdf 'osdem.pdf' 'colab.png' 'output4.pdf' --opacity=0.3 --scale-width";

        //guardar el nombre del archivo en la licencia tambien
        $licencia = Licencia::find($id);
        if ($licencia->archivo_adjunto !=null)
        {
            $rutaArchivoPDF = ".".$licencia->archivo_adjunto; 
            $aprobado = "lincenciaId-$id-aprobada.pdf";
            $rutaLicenciaAprobada = "../aprobados/$aprobado";
            //dd($rutaArchivoPDF);

            try
            {
                $ejecuta = exec("./aprobados/markpdf '$rutaArchivoPDF' 'aprobados/ultimos-libros.png' 'aprobados/$aprobado' --opacity=0.3 --scale-width");
        
                
                
                return "Licencia Aprobada!!! Mensaje ---> $ejecuta <br><br> <a href='$rutaLicenciaAprobada'>Ver archivo</a>";

            }catch(exception $error)
            {
                echo json_encode(array('resultado'=>'<h1>Ocurrió un error. Será reedirigido<h1>'));
                //echo ('ERROr catch envio mail');
    
            }

            
            
        }
        else{
            return "La licencia no posee adjunto todavia.";
        }
        
    }
}
