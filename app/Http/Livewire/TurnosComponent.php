<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Turno;
use App\Models\User;

class TurnosComponent extends Component
{
    public $horarios = ['9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '14:00', '14:30', '15:00'];
    public $fecha, $hora, $usuarioID, $domingo;
    public $mensaje ="";
    public $dias = array("domingo","lunes","martes","miércoles","jueves","viernes","sábado");
    
    public function render()
    {
        $this->usuarioID = auth()->id();
        $turnos = Turno::latest();

        return view('livewire.turnos-component', ['turnos'=>$turnos]);
    }

    public function crearTurno()
    {   
        if (($this->fecha == "") || ($this->hora== "") || ($this->hora== "seleccione"))
       {
            $this->mensaje = "Faltan datos para poder cargar el turno.";
       }
       elseif (($this->fecha != "") || ($this->hora!= "") || ($this->hora!= "seleccione"))
       {
            $this->mensaje = "ok";
            $this->domingo = $this->dias[date("w")];
       }
       else
       {
            $this->mensaje = "";
       }

    }
}
