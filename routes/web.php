<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\LicenciaController;
use App\Http\Controllers\PermisoController;
use Illuminate\Support\Facades\DB;


#Al ingresar
//Route::get('/', [UsuarioController::class, 'indexUsuario'])->middleware('auth');
Route::get('/', function () {
    return view ('usuario.turnos');
})->middleware('auth');

#Del usuario
Route::get('/datosUsuario', [UsuarioController::class, 'datosUsuario'])->middleware('auth');
Route::post('/modificaUsuario', [UsuarioController::class, 'modificaUsuario'])->middleware('auth');

#Mis tramites -- ver como desplegarlas si todas juntas o por separado
Route::get('/misLicencias', [LicenciaController::class, 'misLicencias'])->middleware('auth');
Route::get('/misPermisos', [PermisoController::class, 'misPermisos'])->middleware('auth');

#Del trámite Licencias
Route::get('/nuevaLicencia', [LicenciaController::class, 'nuevaLicencia'])->middleware('auth');
Route::post('/recibeLicencia', [LicenciaController::class, 'recibeLicencia'])->middleware('auth');
Route::get('/modificaTramiteLicencia', [LicenciaController::class, 'modificaTramiteLicencia'])->middleware('auth');
Route::post('/recibeModificarTramiteLicencia', [LicenciaController::class, 'recibeModificarTramiteLicencia'])->middleware('auth');

#Del tramite permisos
Route::get('/nuevoPermiso', [PermisoController::class, 'nuevoPermiso'])->middleware('auth');
Route::post('/recibePermiso', [PermisoController::class, 'recibePermiso'])->middleware('auth');
Route::get('/modificaTramitePermiso', [PermisoController::class, 'modificaTramitePermiso'])->middleware('auth');
Route::post('/recibeModificarTramitePermiso', [PermisoController::class, 'recibeModificarTramitePermiso'])->middleware('auth');




#aprobar Licencia con marca de agua --en preparacion
Route::get('aprobar/', function () {
    $tramites = DB::table('licencias as lic')
        ->join('users AS usu', 'lic.id_usuario', '=', 'usu.id')
        ->select('lic.id AS idLicencia', 'lic.tipo_tramite AS tramite', 'lic.sector_pertenencia AS sector', 'lic.fecha_tramite',
        'lic.licencia_desde', 'lic.licencia_hasta', 'lic.archivo_adjunto', 
        'usu.name AS nombre', 'usu.email AS correo', 'usu.socio AS socio')
        ->get();

        return view ('admin.aprobar-licencia', ['tramites'=> $tramites]);
    
});
Route::get('/aprobarLicencia/{ide}', [LicenciaController::class, 'aprobarLicencia']);


#Del Administrador
Route::get('/adminDatosUsuario', [AdministradorController::class, 'adminDatosUsuario'])->middleware('auth');
Route::get('/recibeDatosUsuarioAdmin', [AdministradorController::class, 'recibeDatosUsuarioAdmin'])->middleware('auth');
Route::get('/buscaUsuario', [AdministradorController::class, 'buscaUsuario'])->middleware('auth');
//Route::post('/editaUsuAdmin/{ide}', [AdministradorController::class, 'editaUsuAdmin'])->middleware('auth');//esto es para un solo resUltado, cuando quiero ir a editarlo
Route::get("/modificaUsuarioAdmin", [AdministradorController::class, 'modificaUsuarioAdmin'])->middleware('auth');
Route::post("/eliminaUsuario", [AdministradorController::class, 'eliminaUsuario'])->middleware('auth');
Route::get('/adminTramites', [AdministradorController::class, 'adminTramites'])->middleware('auth');

Route::get("/adminPermisosPorUsuario", [AdministradorController::class, 'adminPermisosPorUsuario'])->middleware('auth');
Route::get("/adminLicenciasPorUsuario", [AdministradorController::class, 'adminLicenciasPorUsuario'])->middleware('auth');



require __DIR__.'/auth.php';
