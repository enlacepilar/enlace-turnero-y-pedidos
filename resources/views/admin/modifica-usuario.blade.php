@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4 alert-info p-3">
   
  
    <h1>Modificar datos del usuario</h1>
    <form action="/modificaUsuarioAdmin" method="GET">
    @csrf
       
            <input type="hidden" name="id" value="{{$usu->id}}">
            
            <label for="nombre">Nombre completo:</label>
            <input type="text" id="nombre" name="nombre" class="form-control" required value="{{$usu->name}}"> 
            
            <br>
             
            <label for="direccion">Dirección:</label>
            <input type="text" id="direccion" name="direccion" class="form-control" required value="{{$usu->direccion}}"> 
            
            <br>

            <label for="telefono">telefono Nro.:</label>
            <input type="text" id="telefono" name="telefono" class="form-control" required value="{{$usu->telefono}}"> 
            
            <br>

            <label for="dni">DNI Nro.:</label>
            <input type="text" id="dni" name="dni" class="form-control" required value="{{$usu->dni}}"> 
            
            <br>

            <label for="cuil">Cuil Nro.:</label>
            <input type="text" id="cuil" name="cuil" class="form-control" required value="{{$usu->cuil}}"> 
            
            <br>

            <label for="socio">Legajo Nro.:</label>
            <input type="text" id="socio" name="socio" class="form-control" required value="{{$usu->socio}}"> 
            
            <br>

            <label for="correo">Correo:</label>
            <input type="text" id="correo" name="correo" class="form-control" required value="{{$usu->email}}"> 
            
            <br>
            
            <label for="rol">Rol:</label>
            <select name="rol" id="rol" class="form-control">
                @if ($usu->rol == 'usuario')
                    <option value="usuario" selected>Usuario</option>
                    <option value="admin">Administrador</option>  
                @elseif ($usu->rol == 'admin')
                    <option value="usuario">Usuario</option>
                    <option value="admin" selected>Administrador</option>
                @endif
            </select>

        <br>
        <input type="submit" value="Actualizar" class="btn btn-warning btn-block">
    </form>
</div>
@endsection