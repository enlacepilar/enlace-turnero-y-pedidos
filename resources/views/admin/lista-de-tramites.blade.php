@extends('layouts.app-admin')
@section('contenido_app')
<div class="container mt-4">
    <h1>Listado de trámites registrados</h1>

    <table id="tabla_resultados" class="table table-info">
        <thead>
            <th>Usuario</th>
            <th>Correo</th>
            <th>Nro. socio</th>
            <th>Sector</th>
            <th>Tramite</th>
            <th>Fecha tramite</th>
            <th>Licencia desde</th>
            <th>Licencia hasta</th>
            <th>Archivo adjunto</th>


        </thead>
        @foreach ($tramites as $tramite)
            <tr>
                <td>{{$tramite->nombre}}</td>
                <td>{{$tramite->correo}}</td>
                <td>{{$tramite->socio}}</td>
                <td>{{$tramite->sector}}</td>
                <td>{{$tramite->tramite}}</td>
                <td>{{$tramite->fecha_tramite}}</td>
                <td>{{$tramite->licencia_desde}}</td>
                <td>{{$tramite->licencia_hasta}}</td>
                @if ($tramite->archivo_adjunto)
                <td><a href="{{$tramite->archivo_adjunto}}" target=_blank>Archivo adjunto</a></td>
                @else
                <td>Sin adjunto</td>
                @endif
            </tr>
        
        @endforeach
    </table>

</div>
    @endsection