@extends('layouts.app-usuario')
@section('contenido_app')

<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif
    <h3 id="titulo">Nueva Licencia</h3>
    <h5>Usuario: {{$usuario->name}} | Correo-electrónico: {{$usuario->email}}</h5>
    <form action="/recibeLicencia" class="form-group" method="POST" enctype="multipart/form-data">
    @csrf
        
        <input type="hidden" name="id_usuario" value="{{$usuario->id}}">

        <label for="tipo_tramite" class="mt-4">Seleccione Tipo de permiso/orden de salida</label>
        <select name="tipo_tramite" id="tipo_tramite" class="form-control">
            <option value="Licencia Anual">Licencia Anual</option>
            <option value="Licencia Especial">Licencia Especial</option>
   
        </select>

        <label for="sector_pertenencia" class="mt-4">SECTOR AL QUE PERTENCE</label>
        <input type="text" id="sector_pertenencia" name="sector_pertenencia" class="form-control" placeholder="Escriba su respuesta" required>

        <label for="fecha_tramite" class="mt-4">FECHA DE TRAMITE: <i class="far fa-calendar-alt"></i></label>
        <input type="date" id="fecha_tramite" class="form-control" name="fecha_tramite" required>
        <div style="display:inline-block;"> @error('fecha_tramite') <span class="error text-danger">{{ $message }}</span> @enderror </div>
        
        <!-- <br>
        <label for="imagen" class="mt-4">Imagen de Tapa (máximo 1MB)</label>
        <input type="file" name="imagen" id="imagen" class="alert-success form-control" value="Subir imagen">
        @error('imagen') <span class="error text-danger">{{ $message }}</span> @enderror

        <label for="enlace_libro" class="mt-4">Subir Libro en PDF (máximo 5MB)</label>
        <input type="file" accept="application/pdf" name="enlace_libro" id="enlace_libro" class="alert-success form-control" required>
        @error('enlace_libro') <span class="error text-danger">{{ $message }}</span> @enderror  -->
        <input type="submit" value="Enviar" class="form'control mt-3 btn btn-block btn-info">
    </form>

</div>
@endsection