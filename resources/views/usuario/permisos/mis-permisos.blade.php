@extends('layouts.app-usuario')
@section('contenido_app')
<div class="container mt-4 animate__animated animate__lightSpeedInLeft">
    <h1 id="titulo">Lista de permisos solicitados</h1>

    <table class="table table-info table-bordered table-hover" id="tabla_resultados">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Archivo</th>
                <th>Inicio permiso</th>
                <th>Fin</th>
                <th>Modificar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->fecha_tramite))}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                <td>{{$tramite->sector_pertenencia}}</td>
                @if (isset($tramite->archivo_adjunto))
                <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                <td>Sin archivo adjunto</td>
                @endif
                
                @if (isset($tramite->licencia_desde))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_desde))}}</td>
                @else
                    <td>Sin fecha de inicio</td>
                @endif
               
                @if (isset($tramite->licencia_hasta))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_hasta))}}</td>
                @else
                    <td>Sin fecha de fin</td>
                @endif

            <form action="/modificaTramitePermiso">
            @csrf
                <input type="hidden" name="idTramite" value="{{$tramite->id}}"">
                <td class="text-center"><button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
            </form>
                <!-- <td class="text-center"><a href="/modificaTramite/{{$tramite->id}}" class="btn btn-outline-warning btn-"><i class="fas fa-edit"></i></a></td> -->
            @endforeach   
                </tr>
            
        </tbody>
    
    </table>
</div>
<!-- <script>
let eliminaLibro = async (idLibro)=>
{
    console.log ("El resultado es" + idLibro)
    if (confirm("¿Seguro que querés borrar el libro?"))
    {
        const url = '/eliminaLibro'
        const request = new Request(url, 
        {
            method: 'POST',
            body: JSON.stringify({"idLibro": idLibro}),
            cache: "no-cache",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        
        });
        const response = await fetch(request);
        const data = await response.json();       
        console.log(data)
        Swal.fire(
            {
                type: 'success',
                title: data,
                showConfirmButton: false,
                timer: 1500
            })
        location.reload()
        
    }
    
        
}
</script> -->
@endsection
