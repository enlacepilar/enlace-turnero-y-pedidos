<div class='container alert-warning'>
    {{-- Because she competes with no one, no one can compete with her. --}}
    Buena mierda, hermano, estas con los turnos aplicados

    <h2>La fecha es = {{$fecha}}</h2>
    <h2>La hora es = {{$hora}}</h2>
    <h2>Domingo = {{$domingo}}</h2>
    <div class="row">
        <div class='col-5'>
            <label class="display-block" for="fecha">Seleccione un dia para el turno</label>
            <input class="form-control" type="date" wire:model.defer="fecha" id="fecha">
        
        
        </div>
        
        <div class='col-5'>
            <label class="display-block" for="hora">Seleccione un horario disponible</label>
            <select class="form-control" name="hora" id="hora" wire:model.defer="hora">
            <option value="seleccione">Seleccione</option>
                @foreach ($horarios as $hora)
                    <option value="{{$hora}}">{{$hora}}</option>
                @endforeach
            </select>
        </div>

    </div>

    <div class="text-right">
        <button class="mt-3 btn btn-info"wire:click="crearTurno">Agregar</button>
    </div>

    @if ($mensaje=="")
        <p></p>    
    @elseif ($mensaje=="ok")
        <p class="p-1 alert-success animate__animated animate__bounceInRight text-center">Turno cargado correctamente.</p>
    
    @else
        <p class="p-1 alert-danger animate__animated animate__bounceInRight">{{$mensaje}}</p>
    @endif 
   
</div>
