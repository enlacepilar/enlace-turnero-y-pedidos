@extends('errors::minimal')

@section('title', __('La página expiró'))
@section('code', '419')
@section('message', __('El tiempo de consulta expiró. Refresque con F5'))
